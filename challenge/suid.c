#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

/* gcc -m32 -o ch11 ch11.c */

int 			main(void) 
{
	extern char		**environ;
	extern int		errno;
	char * const	arg[] = {"cat", "suid.c"};

	printf("\nUID %d - EUID %d\n\n", getuid(), geteuid());
	execve("/bin/cat", arg, environ);
	return (errno);
}
