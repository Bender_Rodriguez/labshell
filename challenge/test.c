#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

int                             main(void)
{
	extern char                     **environ;
	//extern int                      errno;
	char * const					arg[] = {"ls", NULL};

	/*
	arg[0] = "ls";
	arg[1] = "test.c";
	*/

	execve("/bin/ls", arg, environ);
	printf("Error :EFAULT %d\n", errno);
	return (errno);
}

