#!/bin/bash

if [ $1 ]
then
    class = $1
    touch $1.cpp ; touch $1.hpp;
else
    read -p 'Entrez un nom  de classe: ' class
    touch $class.cpp ; touch $class.hpp;
fi

echo -e "# ifndef ${class^^}_HPP" > $class.hpp;
echo -e "#define ${class^^}_HPP\n" >> $class.hpp;
echo -e "#include <iostream>\n" >> $class.hpp;
echo -e "class  $class\n{\n\tpublic:\n" >> $class.hpp;
echo -e "\t$class( void );\n\t$class( int const n);" >> $class.hpp;
echo -e "\t$class( $class const & src );\n\t~$class( void );\n" >> $class.hpp;
echo -e "\t$class &\toperator=( $class const & src );" >> $class.hpp;
echo -e "\tgetFoo ( void ) const;\n\n\tprivate:\n\n\tint\t_foo;\n};" >> $class.hpp;

echo -e "#include <iostream>\n#include \"$class.hpp\"\n" >> $class.cpp;
echo -e "$class::$class( void ): _foo(0) {}\n" >> $class.cpp;
echo -e "$class::$class( int const n ): _foo(n) {}\n" >> $class.cpp;
echo -e "$class::$class( $class const & src )\n{\n\t*this = src\n}\n" >> $class.cpp;
echo -e "$class::~$class( void ) {}\n" >> $class.cpp;
echo -e "$class &\t$class::operator=( $class const & rhs )\n{" >> $class.cpp;
echo -e "\tif ( this != &rhs )\n\t\tthis->foo = rhs.getFoo();" >> $class.cpp;
echo -e "\treturn *this;\n}" >> $class.cpp;
clear
